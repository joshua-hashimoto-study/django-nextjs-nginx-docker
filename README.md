# Docker + Django + Next.js + Nginx

## docker-compose.yml

```yml
version: "3"

services:
    nginx:
        restart: always
        build:
            dockerfile: Dockerfile
            context: ./nginx
        ports:
            - "5000:80"
        volumes:
            - static_volume:/app/staticfiles  # for django static files
            - media_volume:/app/mediafiles  # for django media files
    backend:
        build:
          dockerfile: Dockerfile
          context: ./backend
        command: gunicorn core.wsgi -b 0.0.0.0:8000 # gunicorn対応
        environment:
            APPLICATION_NAME: django-nextjs-nginx-docker
            ENVIRONMENT: development
            SECRET_KEY:
            DEBUG: 1
        volumes: 
            - ./backend:/app
            - static_volume:/app/staticfiles  # for django static files
            - media_volume:/app/mediafiles  # for django media files
        ports: 
            - 8000:8000
        depends_on:
            - db
    db:
        image: postgres:11
        volumes:
            - postgres_data:/var/lib/postgresql/data/
        environment:
            - "POSTGRES_HOST_AUTH_METHOD=trust"
    frontend:
        build:
          dockerfile: Dockerfile.dev
          context: ./frontend
        command: npm run dev
        volumes:
            - /app/node_modules # container内のnode_modulesを使用
            - ./frontend:/app
        ports:
            - 3000:3000
        stdin_open: true

volumes:
    postgres_data:
    static_volume:  # for django static files
    media_volume:  # for django media files
```

---

## Note

### relation between nginx and docker-compose

![nginx-default.conf-and-docker-compose-settings](readme/images/nginx-default.conf-and-docker-compose-settings.png)

### react project needs nginx in its direcotry

![need-nginx-in-react-project-directory](readme/images/need-nginx-in-react-project-directory.png)

---
